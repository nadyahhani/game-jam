extends Node

var enemies_killed = 0
var door_is_open = false
var lives = 3
var collected = []
var gained_legend = false

# Called when the node enters the scene tree for the first time.
func _process(delta):
	if collected.size() == 4:
		get_tree().change_scene("res://Scenes/Ending Menu.tscn")
		restart()

func open_door():
	door_is_open = true
	
func close_door():
	door_is_open = false
	
func add_killed_enemies():
	enemies_killed += 1
	
func reset_killed_enemies():
	enemies_killed = 0
	
func reset():
	enemies_killed = 0
	door_is_open = false

func dead():
	enemies_killed = 0
	door_is_open = false
	collected = []
	if !gained_legend:
		lives = 3
		gained_legend = true
	get_tree().change_scene("res://Scenes/Guiding Menu.tscn")
	
func killed():
	enemies_killed = 0
	lives -= 1
	if lives == -1:
		dead()
		
func restart():
	enemies_killed = 0
	door_is_open = false
	collected = []
	gained_legend = false
	lives = 3
	
func collectGem(gem):
	if !collected.has(gem):
		collected.append(gem)
	print(collected)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass