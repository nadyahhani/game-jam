extends Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _process(delta):
	if (global.gained_legend && global.lives == -1) || global.collected.size() == 4:
		self.text = "RESTART?"