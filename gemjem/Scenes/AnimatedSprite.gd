extends AnimatedSprite

func _process(delta):
	if global.gained_legend:
		self.play("gained")
	else:
		self.play("none")
