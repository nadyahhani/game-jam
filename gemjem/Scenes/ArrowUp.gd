extends AnimatedSprite

export var direction = ""

func _ready():
	if direction == "up":
		self.play("up")
	elif direction == "down":
		self.play("down")
	elif direction == "right":
		self.play("right")
	elif direction == "left":
		self.play("left")