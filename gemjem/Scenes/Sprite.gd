extends Sprite
export var color = ""

func _process(delta):
	if global.collected.has(color):
		show()
	else:
		hide()
