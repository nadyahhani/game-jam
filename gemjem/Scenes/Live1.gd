extends AnimatedSprite

export var live = 3
# Called when the node enters the scene tree for the first time.
func _ready():
	self.play("gone")
	
func _process(delta):
	if global.lives >= live:
		self.play("alive")

