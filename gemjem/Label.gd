extends Label

func _ready():
	Input.action_release("click")

func _process(delta):
	if !global.gained_legend && global.lives == 3:
		self.text = "Each door lead to different rooms, collect the 4 gems, escape the dungeon. Touch the blue circle to punch the Chasers and get the key!"
	elif global.gained_legend && global.lives == 3:
		self.text = "Seems like you're struggling, here's the Legendary Laser Crystal, Touch the blue circle to shoot!"
	elif global.gained_legend && global.lives == -1:
		self.text = "GAME OVER"
	elif global.collected.size() == 4:
		self.text = "You collected all the gems! You escaped the maze!"
	else:
		self.text = "handle"
	
