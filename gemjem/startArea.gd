extends Area2D

func _process(delta):
	if Input.is_action_pressed("click"):
		get_tree().change_scene("res://Scenes/Level 1.tscn")
		if global.lives == -1:
			global.restart()

func _on_startArea_body_entered(body):
	get_tree().change_scene("res://Scenes/Level 1.tscn")

