extends Area2D

export var scene = "Level 2"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Red_Door_body_entered(body):
	if global.door_is_open:
		get_tree().change_scene("res://Scenes/"+scene+".tscn")
		global.close_door()
