extends Button

var direction

func _ready():
	direction = self.get_parent().direction

func _gui_input(event):
	if event.pressed:
		Input.action_press("ui_" + direction)
	else:
		Input.action_release("ui_" + direction)
