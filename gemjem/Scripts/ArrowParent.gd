extends Area2D

export var direction = "up"
# Called when the node enters the scene tree for the first time.
func _ready():
	Input.action_release("ui_" + direction)
func _on_Arrow_mouse_entered():
	Input.action_press("ui_" + direction)

func _on_Arrow_mouse_exited():
	Input.action_release("ui_" + direction)