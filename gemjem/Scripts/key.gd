extends Area2D
var enabled = false
export var killed = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	hide()

func _process(delta):
	if global.enemies_killed == killed:
		show()
		global.reset_killed_enemies()
		enabled = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_key_body_entered(body):
	if body.name == "Player" && enabled:
		hide()
		global.open_door() 
