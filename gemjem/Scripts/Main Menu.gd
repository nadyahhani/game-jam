extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _process(delta):
	if Input.is_action_just_pressed("click"):
		get_tree().change_scene("res://Scenes/Guiding Menu.tscn")
		Input.action_release("click")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
