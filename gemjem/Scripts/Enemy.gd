extends KinematicBody2D
 
export var MOVE_SPEED = 50
 
onready var raycast = $RayCast2D
 
var player = null
 
func _ready():
    add_to_group("zombies")
    $AnimatedSprite.play()
 
func _physics_process(delta):
    if player == null:
        return
    var vec_to_player = player.global_position - global_position
    vec_to_player = vec_to_player.normalized()
    raycast.global_rotation = atan2(vec_to_player.y, vec_to_player.x)
    move_and_collide(vec_to_player * MOVE_SPEED * delta)
   
    if raycast.is_colliding():
        var coll = raycast.get_collider()
        if coll.name == "Player":
            coll.kill()
 
func kill():
    queue_free()
    global.add_killed_enemies()
 
func set_player(p):
    player = p