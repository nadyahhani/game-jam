extends Area2D

export var level = "Level 1"
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Green_Door_body_entered(body):
	if global.door_is_open:
		get_tree().change_scene("res://Scenes/" + level + ".tscn")
