extends KinematicBody2D
 
const MOVE_SPEED = 100
 
onready var raycast = $RayCast2D
onready var line = $Line2D
onready var anim = $AnimatedSprite

var screen_size
 
func _ready():
    yield(get_tree(), "idle_frame")
    get_tree().call_group("zombies", "set_player", self)
    screen_size = get_viewport_rect().size
    anim.play("idle")
    global.enemies_killed = 0
    if global.gained_legend:
        var temp_vec = Vector2()
        temp_vec.x = 10000
        temp_vec.y = 0
        raycast.cast_to = temp_vec


func _physics_process(delta):
    var move_vec = Vector2()
    if Input.is_action_pressed("ui_up"):
        move_vec.y -= 1
        raycast.global_rotation_degrees = -90
        line.global_rotation_degrees = -90
    if Input.is_action_pressed("ui_down"):
        move_vec.y += 1
        raycast.global_rotation_degrees = 90
        line.global_rotation_degrees = 90
    if Input.is_action_pressed("ui_left"):
        move_vec.x -= 1
        raycast.global_rotation_degrees = 180
        line.global_rotation_degrees = 180
    if Input.is_action_pressed("ui_right"):
        move_vec.x += 1
        raycast.global_rotation_degrees = 0
        line.global_rotation_degrees = 0
    move_vec = move_vec.normalized()
    move_and_collide(move_vec * MOVE_SPEED * delta)
   
    if move_vec.length() > 0:
        move_vec = move_vec.normalized() * MOVE_SPEED
        anim.play("walk")
    elif Input.is_action_pressed("shoot"):
        anim.play("shoot")
        var coll = raycast.get_collider()
        if raycast.is_colliding() and coll.has_method("kill"):
            coll.kill()
    else:
        anim.play("idle")

    if move_vec.x != 0:
        anim.play("walk")
        anim.flip_v = false
        # See the note below about boolean assignment
        anim.flip_h = move_vec.x < 0
 
func kill():
    get_tree().reload_current_scene()
    global.killed()
